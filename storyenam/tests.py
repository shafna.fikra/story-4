from django.test import TestCase
from django.urls import resolve, reverse
from django.test import Client
from .models import Kegiatan, Orang
from django.apps import apps
from .views import addactivity, listactivity, deleteUser, register
from .forms import FormKegiatan, FormOrang
from .apps import StoryenamConfig


class ModelTest(TestCase):
    def setUp(self):
        self.kegiatan = Kegiatan.objects.create(
            nama_kegiatan="belajar", deskripsi_kegiatan="belajar PPW")
        self.orang = Orang.objects.create(nama_orang="Pucca")

    def test_instance_created(self):
        self.assertEqual(Kegiatan.objects.count(), 1)
        self.assertEqual(Orang.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.kegiatan), "belajar")
        self.assertEqual(str(self.orang), "Pucca")

class UrlsTest(TestCase):
    def setUp(self):
        self.kegiatan = Kegiatan.objects.create(
            nama_kegiatan="belajar", deskripsi_kegiatan="belajar PPW")
        self.orang = Orang.objects.create(
            nama_orang="Garu", kegiatan=Kegiatan.objects.get(nama_kegiatan="belajar"))
        self.listactivity = reverse("listactivity")
        self.addactivity = reverse("addactivity")
        self.register = reverse("register", args=[self.kegiatan.pk])
        self.delete = reverse("delete", args=[self.orang.pk])

    def test_listActivity_use_right_function(self):
        found = resolve(self.listactivity)
        self.assertEqual(found.func, listactivity)

    def test_addActivity_use_right_function(self):
        found = resolve(self.addactivity)
        self.assertEqual(found.func, addactivity)

    def test_register_use_right_function(self):
        found = resolve(self.register)
        self.assertEqual(found.func, register)

    def test_delete_use_right_function(self):
        found = resolve(self.delete)
        self.assertEqual(found.func, deleteUser)


class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.listactivity = reverse("listactivity")
        self.addactivity = reverse("addactivity")

    def test_GET_listActivity(self):
        response = self.client.get(self.listactivity)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'listactivity.html')

    def test_GET_addActivity(self):
        response = self.client.get(self.addactivity)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'addactivity.html')

    def test_POST_addActivity(self):
        response = self.client.post(self.addactivity,
                                    {
                                        'nama_kegiatan': 'belajar',
                                        'deskripsi_kegiatan ': "belajar PPW"
                                    }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_addActivity_invalid(self):
        response = self.client.post(self.addactivity,
                                    {
                                        'nama_kegiatan': '',
                                        'deskripsi_kegiatan ': ""
                                    }, follow=True)
        self.assertTemplateUsed(response, 'addactivity.html')

    def test_GET_delete(self):
        kegiatan = Kegiatan(nama_kegiatan="abc", deskripsi_kegiatan="CDF")
        kegiatan.save()
        orang = Orang(nama_orang="chocho",
                      kegiatan=Kegiatan.objects.get(pk=1))
        orang.save()
        response = self.client.get(reverse('delete', args=[orang.pk]))
        self.assertEqual(Orang.objects.count(), 0)
        self.assertEqual(response.status_code, 302)

class TestRegist(TestCase):
    def setUp(self):
        kegiatan = Kegiatan(nama_kegiatan="ngoding", deskripsi_kegiatan="pusing")
        kegiatan.save()

    def test_regist_POST(self):
        response = Client().post('/activity/register/1/',
                                 data={'nama_orang': 'pinky'})
        self.assertEqual(response.status_code, 302)

    def test_regist_GET(self):
        response = self.client.get('/activity/register/1/')
        self.assertTemplateUsed(response, 'register.html')
        self.assertEqual(response.status_code, 200)

    def test_regist_POST_invalid(self):
        response = Client().post('/activity/register/1/',
                                 data={'nama': ''})
        self.assertTemplateUsed(response, 'register.html')
        # self.assertEqual(response.status_code, 302)

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(StoryenamConfig.name, 'storyenam')
        self.assertEqual(apps.get_app_config('storyenam').name, 'storyenam')

