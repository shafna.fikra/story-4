from django import forms


class FormKegiatan(forms.Form):
    nama_kegiatan = forms.CharField(
        label="What's your activity?",
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control w-60',
            }
        )
    )

class FormOrang(forms.Form):
    nama_orang = forms.CharField(
        label="Name",
        max_length=64,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
            }
        )
    )
