from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'profile.html')

def experience(request):
    return render(request, 'experience.html')